<?php

/* _global/index.html */
class __TwigTemplate_757c7a5ea68476c4a18e83dd34e7c5fcd6b2d2a79970a14d8713c3421e5b5c2a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'naslov' => array($this, 'block_naslov'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
    <head>
        <title>Ta&amp;Ta - ";
        // line 4
        $this->displayBlock('naslov', $context, $blocks);
        echo "</title>
        <meta charset=\"utf-8\">
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/font-awesome/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\" integrity=\"sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/css/main.css\">
    </head>
    <body>
        <div class=\"container\">
            <header>
                <div class=\"row\">
                    <div class=\"col col-sm\">
                        <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\">
                            <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/banner.jpg\" alt=\"Banner\">
                        </a>
                    </div>

                    <div class=\"col col-sm\">
                        <div class=\"social-icons text-right\">
                            <a href=\"#\"><i class=\"fab fa-linkedin\"></i></a>
                            <a href=\"#\"><i class=\"fab fa-facebook-square\"></i></a>
                            <a href=\"#\"><i class=\"fab fa-twitter-square\"></i></a>
                            <a href=\"#\"><i class=\"fab fa-google-plus-g\"></i></a>
                            <a href=\"#\"><i class=\"fab fa-youtube\"></i></a>
                        </div>

                        <div class=\"search-box\">
                            <form method=\"post\" action=\"";
        // line 31
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "search\">
                                <div class=\"form-group\">
                                    <div class=\"input-group\">
                                        <input type=\"text\" name=\"q\" class=\"form-control\"
                                               placeholder=\"Ključne reči pretrage\" required pattern=\"[^\\s]{2,}\">
                                        <div class=\"input-group-append\">
                                            <button type=\"submit\" class=\"btn btn-primary\">
                                                <i class=\"fa fa-search\"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <nav class=\"navbar navbar-expand-lg navbar-light bg-light main-menu\">
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">
                        <div class=\"navbar-nav\">
                            <a class=\"nav-item nav-link\" href=\"";
        // line 55
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\"><i class=\"fas fa-home\"></i> Pocetna</a>
                            <a class=\"nav-item nav-link\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "categories\"><i class=\"fas fa-list-ul\"></i> Kategorije</a>
                            <a class=\"nav-item nav-link\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/profile\"><i class=\"fas fa-user\"></i> Profil</a>
                            <a class=\"nav-item nav-link\" href=\"";
        // line 58
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "contact\"><i class=\"fas fa-phone\"></i> Kontakt</a>
                            <a class=\"nav-item nav-link\" href=\"";
        // line 59
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/logout\"><i class=\"fas fa-sign-out-alt\"></i> Odjava</a>
                        </div>
                    </div>
                </nav>
            </header>

            <main>
                ";
        // line 66
        $this->displayBlock('main', $context, $blocks);
        // line 68
        echo "            </main>

            <div class=\"card bookmark-card\">
                <div class=\"card-body\">
                    <a href=\"#\" onclick=\"clearBookmarks();\">Clear bookmarks</a>
                </div>
                <div class=\"bookmarks card-body\">
                    Please wait, loading bookmarks...
                </div>
            </div>

            <footer class=\"site-footer text-center\">
                &copy; 2018 - Aukcijska kuca Ta&amp;Ta
            </footer>
        </div>

        <script>const BASE = '";
        // line 84
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "';</script>
        <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/jquery/dist/jquery.min.js\"></script>
        <script src=\"";
        // line 86
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/js/bootstrap.min.js\"></script>
        <script src=\"";
        // line 87
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/bookmark.js\"></script>
        <script src=\"";
        // line 88
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/js/offer.js\"></script>
    </body>
</html>";
    }

    // line 4
    public function block_naslov($context, array $blocks = array())
    {
        echo "Pocetna";
    }

    // line 66
    public function block_main($context, array $blocks = array())
    {
        // line 67
        echo "                ";
    }

    public function getTemplateName()
    {
        return "_global/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 67,  177 => 66,  171 => 4,  164 => 88,  160 => 87,  156 => 86,  152 => 85,  148 => 84,  130 => 68,  128 => 66,  118 => 59,  114 => 58,  110 => 57,  106 => 56,  102 => 55,  75 => 31,  58 => 17,  54 => 16,  44 => 9,  39 => 7,  35 => 6,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_global/index.html", "C:\\xampp\\htdocs\\views\\_global\\index.html");
    }
}
