<?php

/* UserAuctionManagement/getAdd.html */
class __TwigTemplate_40393a151cd92671bf2faedaddf0dcbf2c427c7d486dc269b4411021d3ead590 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserAuctionManagement/getAdd.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div>
    <div class=\"options\">
        <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/auctions\">List my auctions</a>
    </div>

    <form class=\"auctions-form\" method=\"POST\" enctype=\"multipart/form-data\">
        <div class=\"form-group\">
            <label for=\"title\">Title: </label>
            <input type=\"text\" id=\"title\" name=\"title\" required class=\"form-control\" pattern=\".*[^\\s]{3,}.*\">
        </div>

        <div class=\"form-group\">
            <label for=\"description\">Description: </label>
            <textarea id=\"description\" name=\"description\" required class=\"form-control\" rows=\"10\" pattern=\".*[^\\s]{3,}.*\"></textarea>
        </div>

        <div class=\"form-group\">
            <label for=\"starting_price\">Starting price: </label>
            <input type=\"number\" id=\"starting_price\" name=\"starting_price\" required class=\"form-control\"
                   min=\"0.01\" step=\"0.01\">
        </div>

        <div class=\"form-group\">
            <label for=\"starts_at\">Starts at: </label>
            <input type=\"datetime-local\" id=\"starts_at\" name=\"starts_at\" required class=\"form-control\">
        </div>

        <div class=\"form-group\">
            <label for=\"ends_at\">Ends at: </label>
            <input type=\"datetime-local\" id=\"ends_at\" name=\"ends_at\" required class=\"form-control\">
        </div>

        <div class=\"form-group\">
            <label for=\"category_id\">Auction category: </label>
            <select class=\"form-control\" name=\"category_id\" id=\"category_id\">
                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 40
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "category_id", array()), "html", null, true);
            echo "\">
                    ";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", array()), "html", null, true);
            echo "
                </option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "            </select>
        </div>

        <div class=\"form-group\">
            <label for=\"image\">Image: </label>
            <input type=\"file\" id=\"image\" name=\"image\" required class=\"form-control\" accept=\"image/jpeg, image/png\">
        </div>

        <div class=\"form-group\">
            <button type=\"submit\" class=\"btn btn-primary\">
                <i class=\"fa fa-plus\"></i>
                Add auction
            </button>
        </div>
    </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "UserAuctionManagement/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 44,  84 => 41,  79 => 40,  75 => 39,  39 => 6,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserAuctionManagement/getAdd.html", "C:\\xampp\\htdocs\\views\\UserAuctionManagement\\getAdd.html");
    }
}
