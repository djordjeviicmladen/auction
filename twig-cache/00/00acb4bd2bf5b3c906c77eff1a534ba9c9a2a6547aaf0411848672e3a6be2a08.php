<?php

/* UserCategoryManagement/getAdd.html */
class __TwigTemplate_0db44021bd5e38e0a4308ffbc7b0aa1c3f23a80ab0b417447edce2e3bf93eb34 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserCategoryManagement/getAdd.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div>
    <div class=\"options\">
        <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/categories\">List all categories</a>
    </div>

    <form class=\"category-form\" method=\"POST\">
        <div>
            <label for=\"name\">Name: </label>
            <input type=\"text\" id=\"name\" name=\"name\" required>
        </div>

        <div>
            <button type=\"submit\">
                Add category
            </button>
        </div>
    </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "UserCategoryManagement/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserCategoryManagement/getAdd.html", "C:\\xampp\\htdocs\\views\\UserCategoryManagement\\getAdd.html");
    }
}
