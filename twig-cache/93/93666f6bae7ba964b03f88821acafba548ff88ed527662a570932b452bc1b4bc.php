<?php

/* UserAuctionManagement/getEdit.html */
class __TwigTemplate_5689bc340cfa65193782630d54895d2330fe5dd00aea242a5fa48e8d7fc66ea2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "UserAuctionManagement/getEdit.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div>
    <div class=\"options\">
        <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/auctions\">List my auctions</a>
    </div>

    <form class=\"auctions-form\" method=\"POST\" enctype=\"multipart/form-data\" onsubmit=\"return validateForm();\">
        <div class=\"form-group\">
            <label for=\"title\">Title: </label>
            <input type=\"text\" id=\"title\" name=\"title\" required class=\"form-control\"
                   value=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "title", array()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"description\">Description: </label>
            <textarea id=\"description\" name=\"description\" required class=\"form-control\" rows=\"10\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "description", array()), "html", null, true);
        echo "</textarea>
        </div>

        <div class=\"form-group\">
            <label for=\"starting_price\">Starting price: </label>
            <input type=\"number\" id=\"starting_price\" name=\"starting_price\" required class=\"form-control\"
                   min=\"0.01\" step=\"0.01\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "starting_price", array()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"starts_at\">Starts at: </label>
            <input type=\"datetime-local\" id=\"starts_at\" name=\"starts_at\" required class=\"form-control\"
                   value=\"";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "starts_at", array()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"ends_at\">Ends at: </label>
            <input type=\"datetime-local\" id=\"ends_at\" name=\"ends_at\" required class=\"form-control\"
                   value=\"";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "ends_at", array()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"category_id\">Auction category: </label>
            <select class=\"form-control\" name=\"category_id\" id=\"category_id\">
                ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 43
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "category_id", array()), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "category_id", array()) == twig_get_attribute($this->env, $this->source, $context["category"], "category_id", array()))) {
                echo "selected";
            }
            echo ">
                    ";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", array()), "html", null, true);
            echo "
                </option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </select>
        </div>

        <div class=\"form-group\">
            <label for=\"image\">Image: </label>
            <input type=\"file\" id=\"image\" name=\"image\" class=\"form-control\" accept=\"image/jpeg, image/png\">
        </div>
    
        <div class=\"form-group\">
            <button type=\"submit\" class=\"btn btn-primary\">
                <i class=\"fa fa-pencil\"></i>
                Edit auction
            </button>
        </div>

        <div class=\"alert alert-warning d-none\" id=\"error-message\"></div>
    </form>

    <script>
        function validateForm() {
            let status = true;

            document.querySelector('#error-message').classList.add('d-none');
            document.querySelector('#error-message').innerHTML = '';

            const title = document.querySelector('#title').value;
            if (!title.match(/.*[^\\s]{3,}.*/)) {
                document.querySelector('#error-message').innerHTML += 'The title must contain at least three visible characters...<br>';
                document.querySelector('#error-message').classList.remove('d-none');
                status = false;
            }

            const description = document.querySelector('#description').value;
            if (!description.match(/.*[^\\s]{7,}.*/)) {
                document.querySelector('#error-message').innerHTML += 'The description must contain at least seven visible characters...<br>';
                document.querySelector('#error-message').classList.remove('d-none');
                status = false;
            }

            return status;
        }
    </script>
</div>
";
    }

    public function getTemplateName()
    {
        return "UserAuctionManagement/getEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 47,  106 => 44,  97 => 43,  93 => 42,  84 => 36,  75 => 30,  66 => 24,  57 => 18,  49 => 13,  39 => 6,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "UserAuctionManagement/getEdit.html", "C:\\xampp\\htdocs\\views\\UserAuctionManagement\\getEdit.html");
    }
}
